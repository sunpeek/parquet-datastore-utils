import pytest
import datetime as dt

from parquet_datastore_utils import read, write, delete_between


def test_read(df_1month, url, partition_cols):
    expected = 44640
    assert len(df_1month) == expected

    write(df_1month, url, partition_cols)
    df = read(url)

    assert len(df) == expected


@pytest.mark.parametrize('start, end, expected', [
    ('2017-05-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00', 1441),
    ('2017-05-10 00:00:00+01:00', '2017-05-10 00:00:00+01:00', 1),
    ('2017-04-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00', 14401),
    ('2017-02-10 00:00:00+01:00', '2017-06-11 00:00:00+01:00', 44640),
    ('2017-05-10 00:00:00', '2017-05-11 00:00:00', 1441),
    ('2017-05-10 00:00:00', '2017-05-10 00:00:00', 1),
    ('2017-04-10 00:00:00', '2017-05-11 00:00:00', 14461),
    ('2017-02-10 00:00:00', '2017-06-11 00:00:00', 44640),
])
def test_read__start_end(df_1month, url, partition_cols, start, end, expected):
    assert len(df_1month) == 44640

    write(df_1month, url, partition_cols)

    df = read(url,
              start_end=(dt.datetime.fromisoformat(start), dt.datetime.fromisoformat(end)))

    assert len(df) == expected


@pytest.mark.parametrize('start, end, expected', [
    ('2017-05-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00', 1441),
])
def test_read__index(df_1month, url, partition_cols, start, end, expected):
    write(df_1month, url, partition_cols)

    df = read(url,
              columns=['timestamps_UTC'],
              start_end=(dt.datetime.fromisoformat(start), dt.datetime.fromisoformat(end)))

    assert df.shape == (expected, 0)


@pytest.mark.parametrize('start, end', [
    ('2017-05-10 00:00:00+01:00', '2017-05-09 00:00:00+01:00'),
])
def test_read__start_gt_end_raises(df_1month, url, partition_cols, start, end):
    write(df_1month, url, partition_cols)

    with pytest.raises(ValueError,
                       match=f'Timestamp "start" must be equal or less than "end"'):
        read(url,
             start_end=(dt.datetime.fromisoformat(start), dt.datetime.fromisoformat(end)))


@pytest.mark.parametrize('start, end', [
    ('2017-05-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00'),
])
def test_read__duplicate_filter_raises(df_1month, url, partition_cols, start, end):
    write(df_1month, url, partition_cols)

    with pytest.raises(ValueError,
                       match=f'Invalid entry in "filters": Filtering by index column "timestamps_UTC" not allowed.'):
        read(url,
             start_end=(dt.datetime.fromisoformat(start), dt.datetime.fromisoformat(end)),
             filters=[('timestamps_UTC', '>=', '2017-05-10 00:00:00')])
