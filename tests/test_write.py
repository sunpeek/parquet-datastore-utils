import pytest
import pandas as pd
import copy
import datetime

from parquet_datastore_utils import write, read


def test_write_read_tz_aware(df_dummy__tz_aware, url, partition_cols):
    df_in = copy.deepcopy(df_dummy__tz_aware)
    write(df_dummy__tz_aware, url, partition_cols)
    df_out = read(url)

    assert df_in.shape == df_out.shape
    pd.testing.assert_index_equal(df_in.index, df_out.index)


def test_write_read_tz_naive(df_dummy__tz_naive, url, partition_cols):
    write(df_dummy__tz_naive, url, partition_cols)
    df = read(url)
    # Interpreted and returned in UTC

    assert len(df) == len(df_dummy__tz_naive)
    assert df.index.tzinfo is not None


def test_write_new(df_1month, url, partition_cols):
    write(df_1month, url, partition_cols)


def test_overwrite(df_1month, url, partition_cols):
    assert len(df_1month) == 44640

    write(df_1month, url, partition_cols, overwrite_period=True)

    df2 = df_1month.resample('D').first()
    df2['year'] = df2.index.year
    df2['quarter'] = df2.index.quarter
    write(df2, url, partition_cols, overwrite_period=True)

    df_ = pd.read_parquet(url)

    assert len(df_) < 44640


def test_non_overlapping_overwrite_enabled(df_1month, url, partition_cols):
    assert len(df_1month) == 44640

    write(df_1month, url, partition_cols, overwrite_period=True)

    df2 = copy.deepcopy(df_1month)
    df2.index = df_1month.index + datetime.timedelta(days=365)
    df2['year'] = df2.index.year
    df2['quarter'] = df2.index.quarter

    write(df2, url, partition_cols)
    df_ = pd.read_parquet(url)

    assert len(df_) == 44640 * 2
