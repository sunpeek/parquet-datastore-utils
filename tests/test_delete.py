import pytest
import datetime as dt
import pandas as pd

from parquet_datastore_utils import read, write, delete_between


def get_start_end_dict(start_iso: str, end_iso: str):
    iso_dates = [pd.to_datetime(dt.datetime.fromisoformat(x)) for x in [start_iso, end_iso]]
    tuple_for_delete = ({'timestamp': x.to_pydatetime(), 'year': x.year, 'quarter': x.quarter} for x in iso_dates)

    return tuple_for_delete


@pytest.mark.parametrize('start, end, expected', [
    ('2017-05-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00', 43199),
    ('2017-05-10 00:00:00+01:00', '2017-05-10 00:00:00+01:00', 44639),
    ('2017-04-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00', 30239),
    ('2017-02-10 00:00:00+01:00', '2017-06-11 00:00:00+01:00', 0),
    ('2016-02-10 00:00:00+01:00', '2018-06-11 00:00:00+01:00', 0),
    ('2017-05-10 00:00:00', '2017-05-11 00:00:00', 43199),
    ('2017-05-10 00:00:00', '2017-05-10 00:00:00', 44639),
    ('2017-04-10 00:00:00', '2017-05-11 00:00:00', 30179),
    ('2017-02-10 00:00:00', '2017-06-11 00:00:00', 0),
    ('2016-02-10 00:00:00', '2018-06-11 00:00:00', 0),
])
def test_delete__1month(df_1month, url, partition_cols, start, end, expected):
    assert len(df_1month) == 44640

    write(df_1month, url, partition_cols)

    start_dict, end_dict = get_start_end_dict(start, end)
    delete_between(url, start=start_dict, end=end_dict, partition_cols=partition_cols)
    df = read(url)

    assert len(df) == expected


@pytest.mark.parametrize('start, end, expected', [
    ('2017-05-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00', 524159),
    ('2017-05-10 00:00:00+01:00', '2017-05-10 00:00:00+01:00', 525599),
    ('2017-04-10 00:00:00+01:00', '2017-05-11 00:00:00+01:00', 480959),
    ('2017-02-10 00:00:00+01:00', '2017-06-11 00:00:00+01:00', 351359),
    ('2017-02-10 00:00:00+01:00', '2017-11-11 00:00:00+01:00', 131039),
    ('2016-02-10 00:00:00+01:00', '2017-06-11 00:00:00+01:00', 293759),
    ('2017-02-10 00:00:00+01:00', '2018-06-11 00:00:00+01:00', 57600),
    ('2016-02-10 00:00:00+01:00', '2018-06-11 00:00:00+01:00', 0),
])
def test_delete__1year(df_1year, url, start, end, partition_cols, expected):
    # Test for multiple files -> more than 1 quarter, if partitioned by year + quarter
    assert len(df_1year) == 525600

    write(df_1year, url, partition_cols)

    start_dict, end_dict = get_start_end_dict(start, end)
    delete_between(url, start=start_dict, end=end_dict, partition_cols=partition_cols)
    df = read(url)

    assert len(df) == expected


@pytest.mark.parametrize('start, end', [
    ('2017-05-10 00:00:00+01:00', '2017-05-01 00:00:00+01:00'),
])
def test_delete_raises__start_gt_end(df_1month, url, start, end, partition_cols):
    write(df_1month, url, partition_cols)

    d = pd.to_datetime(start)
    start_dict = {'timestamp': d, 'year': d.year, 'quarter': d.quarter}
    d = pd.to_datetime(end)
    end_dict = {'timestamp': d, 'year': d.year, 'quarter': d.quarter}

    with pytest.raises(ValueError, match='Timestamp "start" must be equal or less than "end"'):
        delete_between(url, start=start_dict, end=end_dict, partition_cols=partition_cols)


@pytest.mark.parametrize('start, end', [
    ('2019-01-01 00:00:00', '2021-01-01 00:00:00'),
])
def test_read_empty_dataset(df_dummy__tz_naive, url, start, end, partition_cols):
    # Write data, delete all data, try to read
    write(df_dummy__tz_naive, url, partition_cols)
    start_dict, end_dict = get_start_end_dict(start, end)
    delete_between(url, start=start_dict, end=end_dict, partition_cols=partition_cols)

    df = read(url)
    assert df.empty
