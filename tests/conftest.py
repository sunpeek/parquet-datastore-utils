import pytest
import os
import copy

import numpy as np
import pandas as pd
import sunpeek_exampledata


@pytest.fixture
def url(tmp_path_factory):
    return os.path.join('file:///', tmp_path_factory.mktemp("tmp_test_data"), "test_simple_write")


@pytest.fixture
def partition_cols():
    return ['year', 'quarter']


@pytest.fixture(scope='session')
def df_dummy__tz_aware():
    index = pd.date_range(start=pd.to_datetime('2020-01-01'),
                          end=pd.to_datetime('2020-01-10'),
                          freq='1T',
                          tz='UTC')
    df = pd.DataFrame(np.random.randn(len(index), 5), index=index)
    df['year'] = df.index.year
    df['quarter'] = df.index.quarter

    assert df.index.tzinfo is not None

    return df


@pytest.fixture(scope='session')
def df_dummy__tz_naive(df_dummy__tz_aware):
    df = copy.deepcopy(df_dummy__tz_aware)
    df.index = df.index.tz_localize(None)
    assert df.index.tzinfo is None

    return df


@pytest.fixture(scope='session')
def df_1month():
    df = pd.read_csv(sunpeek_exampledata.DEMO_DATA_PATH_1MONTH, sep=';', index_col=0, parse_dates=True)
    df['year'] = df.index.year
    df['quarter'] = df.index.quarter

    return df


@pytest.fixture(scope='session')
def df_1year():
    df = pd.read_csv(sunpeek_exampledata.DEMO_DATA_PATH_1YEAR, sep=';', index_col=0, parse_dates=True)
    df['year'] = df.index.year
    df['quarter'] = df.index.quarter

    return df

# @pytest.fixture(scope="session")
# def image_file(df, tmp_path_factory):
#     fn = tmp_path_factory.mktemp("tmp_test_data") / "test"
#
#     df['year'] = df.index.year
#     df['quarter'] = df.index.quarter
#
#     df.to_parquet(fn)
#
#     return fn
